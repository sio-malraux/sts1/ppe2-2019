<?php

    include 'include/header.php';
    include 'fonctions/fonctions.php';
    unlogged_only();
    ?>

<header>
    <link rel="stylesheet" href="css/style.css">
</header>

<div class="container">

    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">

                <div class="card-body">
                    <?php

                    session_destroy();

                    if(session_status() == PHP_SESSION_NONE){
                    }else{
                        if ($_SESSION['connexion'] == 1){

                            echo '<div class="alert alert-danger" role="alert">
                        Erreur, mauvais identifiant ou mot de passe
                        <br>
                    </div>';
                            $_SESSION['connexion'] = 0;
                        }
                    }



                    ?>



                    <h5 class="card-title text-center">Connexion à votre compte</h5>
                    <form class="form-signin" method="post" action="fonctions/login.php">
                        <div class="form-label-group">
                            <input type="text" id="login" name="login" class="form-control" placeholder="Identifiant" required autofocus>
                            <label for="login">Identifiant</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe" required>
                            <label for="password">Mot de passe</label>
                        </div>

                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Connexion</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>