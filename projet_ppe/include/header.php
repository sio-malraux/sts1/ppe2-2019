<!DOCTYPE html>
<html>
    <head>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">


   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
         <title>Accueil - Ligue Judoka</title>
    </head>
    <body>

 <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

  <div class="collapse navbar-collapse" id="navb">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compétitions/">Compétitions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/clubs/">Clubs</a>
      </li>
        <li class="nav-item">
            <a class="nav-link" href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/clubs/club.php">Membres Clubs</a>
        </li>
      <li class="nav-item">
        <a class="nav-link" href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/Sponsor/">Sponsors</a>
      </li>
    </ul>

      <div class="offset-4">

      <?php

      if(session_status() == PHP_SESSION_NONE){
        session_start();
    }

      if(isset($_SESSION['auth'])){

          if($_SESSION['role'] == 1){
              echo '<a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compte/parametre.php">
          <button  class="btn btn-info my-2 my-sm-0" type="button">Administrateur</button></a>';
          }else{
              echo '<a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compte/parametre.php">
          <button  class="btn btn-info my-2 my-sm-0" type="button">'.$_SESSION['pseudo'].'</button></a>';
          }


          echo '<a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/fonctions/logout.php">
          <button  class="btn btn-danger my-2 my-sm-0" type="button"> Déconnexion </button></a>';

      }else{
          echo '<a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/connexion.php">
          <button  class="btn btn-success my-2 my-sm-0" type="button">Connexion</button></a>';
      }

      ?>
      </div>
  </div>
</nav>
