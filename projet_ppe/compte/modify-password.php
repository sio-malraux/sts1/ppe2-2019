<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 02/03/2019
 * Time: 15:57
 */

include '../fonctions/fonctions.php';

logged_only();
if(!empty($_POST)){

    if(empty($_POST['password']) || $_POST['password'] != $_POST['confirm_password']){
        header('Location parametre.php');
        $_SESSION['password_change'] = 2;
    }else{
        $user_id = $_SESSION['login'];
        $password= password_hash($_POST['password'], PASSWORD_BCRYPT);

        require '../fonctions/connexion_bdd.php';
        $req = ("UPDATE llj_kata.authentification SET password = '".$password."' WHERE login = '".$user_id."'");
        $conn->exec($req);
        $_SESSION['password_change'] = 1;
        header('Location: parametre.php');
    }
}
?>