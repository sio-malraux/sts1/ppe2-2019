
<?php

include '../include/header.php';
require '../fonctions/connexion_bdd.php';
/*if (!isset($_SESSION['competition_select_m']) )
{
$_SESSION['jury_modification']=0;
}
*/
?>
<div class="container">
        <h2 align="center"><br><div>L'Affectation des entraîneurs comme membre du jury</div></h2>
        <br>
       <div class="panel panel-default">   
        <div class="card-header">
<ul class="nav nav-pills">
  <li class="nav-item"><a class="nav-link <?php if($_SESSION['jury_modification'] != 1 && $_SESSION['jury_modification'] != 3){        echo "active";    }?>" data-toggle="tab" href="#tab_ent">Gestion du jury</a></li>
  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Ajouter">Ajouter</a></li>
  <li class="nav-item"><a class="nav-link <?php if($_SESSION['jury_modification'] == 1){        echo "active";    }?>" data-toggle="tab" href="#Modifier">Modifier</a></li>
  <li class="nav-item"><a class="nav-link <?php if($_SESSION['jury_modification'] == 3){        echo "active";    }?>" data-toggle="tab" href="#Supprimer">Supprimer</a></li> 
</ul>
        </div>
            <div class="tab-content">
   <div id="tab_ent" class="tab-pane fade <?php   if($_SESSION['jury_modification'] != 1 && $_SESSION['jury_modification'] != 3){      echo "show active";    }  ?>">
              <div class=" table-responsive">
                  <table class="table table-striped table-sm">
              <thead>
           
                <tr>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Genre</th>
                  <th>Téléphone</th>
                  <th></th>
                  <th>Compétition</th>
                  <th>Supresssion</th>
                </tr>
              </thead>
              <tbody>
              <form class="form-horizontal"  method="POST" action = "ajout_jury.php">
                  <?php
                  if (isset($_GET['a_suppr']))
                  {
                  $list = explode('/', $_GET['a_suppr']);
                  $res = $conn->query("SELECT entraineur.nom AS nom, entraineur.prenom AS prenom,
                      entraineur.genre AS genre, entraineur.tel AS telephone, 
                      competition.nom AS nom_compet 
                      FROM llj_kata.jury 
                      INNER JOIN llj_kata.entraineur ON jury.licence_pro = entraineur.licence_pro 
                      INNER JOIN llj_kata.competition ON jury.num_kata = competition.num_kata
                      AND jury.annee=competition.annee
                    WHERE jury.licence_pro=".$list[0].
                          "And jury.annee=".$list[2].""
                          . "And jury.num_kata=".$list[1]." ORDER BY competition.nom");
                  $r=$res->fetch();?>
                   <tr>
                  <td><?php echo  $r['nom']; ?></td>  
                   <td><?php echo  $r['prenom'];  ?></td>
                   <td><?php echo  $r['genre'];  ?></td>
                    <td><?php echo  $r['telephone'];  ?></td>
                     <td></td>
                  <td><?php echo   $r['nom_compet'];    ?></td>
                       <td><button type="submit" class="btn btn-danger">Supprimer</button></td>
                </tr>
                <?php
                  }
                 // echo $r['nom'];
                 //if (isset($_GET['a_suppr']))
                   
                  $charlyletableau = $conn->query("SELECT entraineur.nom AS nom, entraineur.prenom AS prenom,
                      entraineur.genre AS genre, entraineur.tel AS telephone, 
                      competition.nom AS nom_compet 
                      FROM llj_kata.jury 
                      INNER JOIN llj_kata.entraineur ON jury.licence_pro = entraineur.licence_pro 
                      INNER JOIN llj_kata.competition ON jury.num_kata = competition.num_kata
                      AND jury.annee=competition.annee
                      ORDER BY competition.nom ");
                  
                      
                          while($michel = $charlyletableau->fetch()){
                              ?>
                     
                                     <tr>
                  <td><?php echo '<option>'.$michel['nom'].'</option>'; ?></td>  
                   <td><?php echo '<option>'.$michel['prenom'].'</option>'; ?></td>
                   <td><?php echo '<option>'.$michel['genre'].'</option>'; ?></td>
                    <td><?php echo '<option>'.$michel['telephone'].'</option>'; ?></td>
                     <td></td>
                     <td><?php echo '<option>'.$michel['nom_compet'].'</option>'; ?></td>
                       <td><button type="submit" class="btn btn-danger">Supprimer</button></td>
                </tr>
                  </form>
                <?php
                    }
                                  
                ?>
                  
                
                


              </tbody>
            </table>
              </div>
       <form class="form-horizontal" action = "tab_pdf.php" method="POST">
            <td><button type="submit" class="btn btn-secondary btn-lg">Génerer PDF</button></td> 
       </form>
          </div>
 
 <div id="Ajouter" class="tab-pane fade">
          <form class="form-horizontal" action = "ajout_jury.php" method="POST">
            <div class="alert alert-secondary">
                <p>Sélection de la compétition :
                <select name="competition_select" id ="competition_select">
       
     <?php
                  
                  $requete = $conn->query("select num_kata,annee,nom as nom_competition,date from llj_kata.competition");
                  
                 while($donnees = $requete->fetch()){
                      echo '<option value ="'.$donnees['num_kata'].'/'.$donnees['annee'].'">'.$donnees['nom_competition']." : ".$donnees['date'].'</option>';
                  }
                  
                
                  ?>
     
     
     
 </select></p>
                    <p>Sélection de l'entraineur :
                    <select  name="liste_entraineur"  id ="liste_entraineur"></p>

       <?php          
          $requete1 = $conn->query("select licence_pro,nom as nom_entraineur,prenom from llj_kata.entraineur");
                  
                 while($donnees = $requete1 ->fetch()){
                      echo '<option value ="'.$donnees['licence_pro'].'">'.$donnees['nom_entraineur']."  ".$donnees['prenom'].'</option>';
                  }
                  
     ?>
 </select>
                         <select name="jury" id ="jury">
                             <option value ="1">1</option>
                             <option value ="2">2</option>
                             <option value ="3">3</option>
                             <option value ="4">4</option>
                             <option value ="5">5</option>
                             <option value ="6">6</option>
                             </select>
                <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <input type="submit" class="btn btn-success" name="ValiderAjout" value="Valider">
                </div>  
              </div>
                 
            </div>
          </form>
     
       </div>
                
  <div id="Modifier" class="tab-pane fade  <?php    if($_SESSION['jury_modification'] == 1){      echo "show active";    } ?>" >     
      <form class="form-horizontal" method="POST" action="modif_jury.php">
            <div class="alert alert-secondary">
                
                <p>Sélection de la compétition :
                <select name="competition_select_m" id ="competition_select_m">
                     <?php
                  
                  $requete = $conn->query("select num_kata,annee,nom as nom_competition,date from llj_kata.competition");
                  
                 while($donnees = $requete->fetch()){
                     if (isset($_SESSION['competition_select_m']))
                     {   
                      echo '<option value ="'.$donnees['num_kata'].'/'.$donnees['annee'].'"'; 
                              if ($_SESSION['competition_select_m']==($donnees['num_kata'].'/'.$donnees['annee']))
                              {
                                  echo 'selected';
                              }
                              echo '>'.$donnees['nom_competition']." : ".$donnees['date'].'</option>';
                  
                     }
                     else {
                      echo '<option value ="'.$donnees['num_kata'].'/'.$donnees['annee'].'">'.$donnees['nom_competition']." : ".$donnees['date'].'</option>';
                        }
                 }?>
                 </select>
                 <!-- <div class="col-sm-offset-2 col-sm-10"> -->
                  <input type="submit" class="btn btn-success" name="Valider" value="Valider">
                <!--</div>--></p>
            </div>
      </form> 
     
     
      <form action='modif_jury.php' method="POST">
 <?php 
    if ($_SESSION['jury_modification']==1 ) 
    {
        $list = explode('/', $_SESSION['competition_select_m']);
       // echo 'T E S T : '.$list[0].' - '.$list[1];
        ?>
    
    <p>Sélection du jury à modifier :
                    <select  name="liste_jury"  id ="liste_jury">

       <?php          
          $requete1 = $conn->query("select e.licence_pro,nom as nom_entraineur,prenom from llj_kata.entraineur e "
                  . "                   inner join llj_kata.jury j on j.licence_pro = e.licence_pro"
                  . "                    where j.num_kata = ".$list[0]." and j.annee=".$list[1].";");
                  
                 while($donnees = $requete1 ->fetch()){
                      echo '<option value ="'.$donnees['licence_pro'].'">'.$donnees['nom_entraineur']."  ".$donnees['prenom'].'</option>';
                  }
                
                
                 ?>
 </select></p>
                     <p>Sélection du nouveau jury :
                    <select  name="liste_nouv_jury"  id ="liste_nouv_jury">

       <?php          
          $requete2 = $conn->query("select licence_pro, nom, prenom from llj_kata.entraineur where licence_pro not in (select e.licence_pro from llj_kata.entraineur e "
                  . "                   inner join llj_kata.jury j on j.licence_pro = e.licence_pro"
                  . "                    where j.num_kata = ".$list[0]." and j.annee=".$list[1].");");
                  
                 while($donnees = $requete2 ->fetch()){
                      echo '<option value ="'.$donnees['licence_pro'].'">'.$donnees['nom']."  ".$donnees['prenom'].'</option>';
                  }
                
                
               ?>
 </select></p>      
                     
                  
                <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <input type="submit" class="btn btn-warning" name="Modifier" value="Modifier">
                </div>  
              </div>
      <?php
    } // fin si
     ?>        
          </form>
  </div>
        
  <div id="Supprimer" class="tab-pane fade  <?php    if($_SESSION['jury_modification'] == 3){      echo "show active";    } ?>" >     
      <form class="form-horizontal" method="POST" action="supprimer_jury.php">
            <div class="alert alert-secondary">
      
               
           <p>Sélection de la compétition :
                <select name="competition_select_s" id ="competition_select_s">
                     <?php
                  
                  $requete = $conn->query("select num_kata,annee,nom as nom_competition,date from llj_kata.competition");
                  
                 while($donnees = $requete->fetch()){
                     if (isset($_SESSION['competition_select_s']))
                     {   
                      echo '<option value ="'.$donnees['num_kata'].'/'.$donnees['annee'].'"'; 
                              if ($_SESSION['competition_select_s']==($donnees['num_kata'].'/'.$donnees['annee']))
                              {
                                  echo 'selected';
                              }
                              echo '>'.$donnees['nom_competition']." : ".$donnees['date'].'</option>';
                  
                     }
                     else {
                      echo '<option value ="'.$donnees['num_kata'].'/'.$donnees['annee'].'">'.$donnees['nom_competition']." : ".$donnees['date'].'</option>';
                        }
                 }?>
                 </select>
                 <!-- <div class="col-sm-offset-2 col-sm-10"> -->
                  <input type="submit" class="btn btn-success" name="ValiderSuppr" value="Valider">
                <!--</div>--></p>
             </div>
            </form> 
     
     
      <form action='supprimer_jury.php' method="POST">
 <?php 
    if ($_SESSION['jury_modification']==3 ) 
    {
        $list = explode('/', $_SESSION['competition_select_s']);
       // echo 'T E S T : '.$list[0].' - '.$list[1];
        ?>
    
    <p>Sélection du jury à supprimer :
                    <select  name="liste_jury"  id ="liste_jury">

       <?php          
          $requete3 = $conn->query("select e.licence_pro,nom as nom_entraineur,prenom from llj_kata.entraineur e "
                  . "                   inner join llj_kata.jury j on j.licence_pro = e.licence_pro"
                  . "                    where j.num_kata = ".$list[0]." and j.annee=".$list[1].";");
                  
                 while($donnees = $requete3 ->fetch()){
                      echo '<option value ="'.$donnees['licence_pro'].'">'.$donnees['nom_entraineur']."  ".$donnees['prenom'].'</option>';
                  }
                
                
    }?>
 </select></p>
           
           <button type="submit" name="suppr" class="btn btn-danger">Supprimer</button>
       </form>
       </div>
      </div>
</div>