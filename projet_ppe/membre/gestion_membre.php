<?php
include '../include/header.php';
require '../fonctions/connexion_bdd.php';
?>
<br>
<div class="container">

<h2 align=center><table width="12%" border ="3" cellspacing="1" cellpadding="1"><tr class="table-primary"><td><div>Membres</div></td><tr></table></h2>
<br>
    <?php

    if ($_SESSION['membre'] == 1){
        echo '<div class="alert alert-info" role="alert">
        <strong>Vous venez de créer un nouveau membre !</strong>
        <br><br>
        Identifiant: '.$_SESSION['idconnexion'].'
        <br>
        Mot de passe temporaire: '.$_SESSION['mot_de_passe'].'
        
        <br>
        <i>Identifiant à envoyé au membre en question...</i>
        <br>
    </div>';
        $_SESSION['membre'] = 0;
    }
    ?>


<form class="form-horizontal" action="formulaire.php">
<p align=left><button type="submit" class="btn btn-success" type= "submit">Gestion</button>
    </form>

<form class="form-horizontal" action="editionLicence.php">
    
    <p align=left><button type="submit" class="btn btn-success" type= "submit">Edition des Licences</button>
</form>

      <div class="table-responsive">
        <table style="font-size:0.8em;" class="table table-striped table-sm">
          <thead>
            <tr>
              <th>N° Licence</th>
              <th>Prénom</th>
              <th>Nom</th>
              <th>E-mail</th>
              <th>N° Club</th>
              <th>Genre</th>
              <th>Téléphone</th>
              <th>Date de naissance</th>
              <th>Code Postal</th>
              <th>Adresse</th>
              <th>Ville</th>
              
            </tr>
          </thead>
          <tbody>
            <tr>
                
              <?php $requete = $conn->query("SELECT * FROM llj_kata.membre ORDER BY licence_m ");
                
                while($affiche = $requete->fetch()){
          ?>
          <tr>
            <td><?php echo $affiche['licence_m']?></td>
            <td><?php echo $affiche['prenom']?></td>
            <td><?php echo $affiche['nom']?></td>
            <td><?php echo $affiche['e_mail']?></td>
            <td><?php echo $affiche['num_club']?></td>
            <td><?php echo $affiche['genre']?></td>
            <td><?php echo $affiche['tel']?></td>
            <td><?php echo $affiche['date_naiss']?></td>
            <td><?php echo $affiche['code_postal']?></td>
            <td><?php echo $affiche['adresse']?></td>
            <td><?php echo $affiche['ville']?></td>
            
          </tr>
          <?php } ?>
          
              
            </tr>
          </tbody>
        </table>
          
      </div>

</div>