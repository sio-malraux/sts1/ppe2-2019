<?php
include '../include/header.php';
echo $_SESSION['membre'];
?>

    <div class="container">
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
    <h2>Formulaire</h2>
    <p class="lead">Nous vous rappelons que seul les entraîneurs et les admins sont autorisés à ajouter, modifier ou supprimer des membres. Si vous trouvez cette page et que vous n'êtes pas un entraîneur, veuillez vous déconnecter.</p>
  </div>


      
      <ul class="nav nav-pills">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Ajouter">Ajouter</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Modifier">Modifier</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Supprimer">Supprimer</a></li>
      </ul>

        <div class="tab-content">
        
        <!-- Onglet Ajouter -->
        <div id="Ajouter" class="tab-pane fade show active">
          <form class="form-horizontal" method="post" action = "insertion.php">
            <div class="alert alert-secondary">

        <br>
    <div class="col-md-8 order-md-1">
      <h4 class="mb-3">Informations personnelles</h4>
      <br>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="Prénom">Prénom</label>
            <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Daniel"  required>
            <div class="invalid-feedback">
              Un prénom valide est requis.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="Nom">Nom</label>
            <input type="text" class="form-control" id="nom" name="nom" placeholder="Dupont"  required>
            <div class="invalid-feedback">
              Un nom valide est requis.
            </div>
          </div>
        </div>
          <div class="mb-3">
            <label for="Âge">Date de naissance</label>
            <input type="text" class="form-control" id="datenaiss" name="datenaiss" placeholder="AAAA-MM-JJ" value="" required>
            <div class="invalid-feedback">
              Une date de naissance valide est requise.
            </div>
          </div>

          <div class="mb-3">
            <label for="genre">Genre</label>
            <select class="form-control" id="genre" name="genre" value="" required>
                <option>Homme</option>
                <option>Femme</option>
                <option>Autres</option>
            </select>
            <div class="invalid-feedback">
              Un âge valide est requis.
            </div>
          </div>

        <div class="mb-3">
          <label for="email">Email <span class="text-muted"></span></label>
          <input type="email" class="form-control" id="email" name="email" placeholder="daniel.dupont@exemple.fr" required>
          <div class="invalid-feedback">
            Veuillez entrer une adresse mail valide.
          </div>
        </div>

      <div class="mb-3">
          <label for="tel">Téléphone</label>
          <input type="text" class="form-control" id="tel" name="tel" placeholder="0243464948" required>
          <div class="invalid-feedback">
            Veuillez rentrer un numéro valide.
          </div>
        </div>
      
      <div class="mb-3">
            <label for="club">Club</label>
            <select class="form-control" id="club" name ="club" value="" >
                <?php

require_once '../fonctions/connexion_bdd.php';

$requete = $conn->query("SELECT * FROM llj_kata.club");

while ($donnees = $requete->fetch()) {
    
    
?>
               
                <option value="<?php
    echo $donnees['num'];
?>"><?php
    echo $donnees['nom'];
?></option>
            
                <?php
}

?>
             
            </select>
            </div>
      
        <div class="mb-3">
          <label for="adresse">Adresse</label>
          <input type="text" class="form-control" id="adresse" name ="adresse" placeholder="12 Rue de la paix" required>
          <div class="invalid-feedback">
            Veuillez rentrer une adresse valide.
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="Ville">Ville</label>
            <input type="text" class="form-control" id="ville" name="ville" placeholder="LE MANS" required>

            </select>
            <div class="invalid-feedback">
              Veuillez saisir une ville.
            </div>
          </div>
        </div>
          <div class="col-md-6 mb-3">
            <label for="code_postal">Code Postal</label>
            <input type="text" class="form-control" id="code_postal" name="code_postal" placeholder="72000" required>
            <div class="invalid-feedback">
              Veuillez saisir un code postal.
            </div>
          </div>

      
        <button class="btn btn-primary btn-lg btn-block" type="submit">Valider les informations</button>
    </div>
    </div>
     </form>
</div>
        <!-- Onglet modifier -->
        <div id="Modifier" class="tab-pane fade">
          <form class="needs-validation" novalidate method="POST">
            <div class="alert alert-secondary">
            
     <br>
    
    
     <div class="col-md-3 mb-3">
                   <label for="numLicence">N° Licence :</label>
                <select class="custom-select d-block w-100"   name="licence"  required>
                    <option value="" selected="selected" > </option>
<?php
$requeteLicence  = "SELECT licence_m FROM llj_kata.membre ORDER BY licence_m";
$resultatLicence = $conn->query($requeteLicence);

while ($numLicence = $resultatLicence->fetch()) {
    
    echo '<option value="' . $numLicence['licence_m'] . '">' . $numLicence['licence_m'] . '</option>';
}
?>  
                
                </select>
                   <form class="needs-validation" novalidate method="POST">
                        <input type="submit" value="Rechercher" >
                   </form>
              </div>
            </div>
          </form>
<form class="form-horizontal" method="post" action = "modification.php">
              <div class="mb-3">
                  
                  <?php
if (isset($_POST["licence"])) {
    
    $requete  = "SELECT membre.licence_m, membre.e_mail, membre.tel, membre.prenom, membre.nom, membre.genre, club.nom AS club, membre.num_club, membre.date_naiss, membre.code_postal, membre.adresse, membre.ville FROM llj_kata.membre INNER JOIN llj_kata.club ON membre.num_club = club.num WHERE licence_m=" . $_POST['licence'] . ";";
    $resultat = $conn->query($requete);
    $x        = $resultat->fetch();
    
 
}
?>
                  
                  <div class="mb-3">
                <label for="licence_m">N° Licence :</label>
                <span name="licence_m" class="input-group-text" id="licence_m"><?php echo $x['licence_m']; $_SESSION['lic']=$x['licence_m']; ?></span>
                </div>
                <label for="prenom">Prénom :</label>
                <input name="prenom" type="text" class="form-control" id="prenom" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['prenom'];} ?> " required>
              </div>
            <div class="mb-3">
                <label for="nom">Nom :</label>
                <input name="nom" type="text" class="form-control" id="nom" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['nom'];} ?>" required>
              </div>
                        <div class="mb-3">
                <label for="genre">Genre :</label>
                <input name="genre" type="text" class="form-control" id="genre" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['genre'];} ?>" required>
              </div>
            <div class="mb-3">
                <label for="e_mail">E-mail :</label>
                <input name="e_mail" type="text" class="form-control" id="e_mail" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['e_mail'];} ?>" required>
            </div>
              <div class="mb-3">
                <label for="tel">Téléphone :</label>
                <input name="tel" type="text" class="form-control" id="tel" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['tel'];} ?>" required>
              </div>
            <div class="mb-3">
              <label for="club">Club :</label>
              <input name="club" type="text" class="form-control" id="club" value="<?php if (isset($_POST["licence"]))
{ echo $x['club'];} ?>" required>
              <div class="invalid-feedback">
              </div>
            </div>
            <div class="mb-3">
              <label for="num_club">N° Club :</label>
              <input name="num_club" type="text" class="form-control" id="num_club" value="<?php if (isset($_POST["licence"]))
{ echo $x['num_club'];} ?>" required>
              <div class="invalid-feedback">
              </div>
            </div>
            <div class="mb-3">
              <label for="date">Date de naissance :</label>
              <input name="date" type="date" class="form-control" id="date" value="<?php if (isset($_POST["licence"]))
{ echo $x['date_naiss'];} ?>" required>
              <div class="invalid-feedback">
              </div>
            </div>
    
            <div class="mb-3">
          <label for="adresse">Adresse</label>
          <input type="text" class="form-control" id="adresse" name ="adresse" value="<?php if (isset($_POST["licence"]))
{ echo $x['adresse'];} ?>" required>
          <div class="invalid-feedback">
            Veuillez rentrer une adresse valide.
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="Ville">Ville</label>
            <input type="text" class="form-control" id="ville" name="ville" value="<?php if (isset($_POST["licence"]))
{ echo $x['ville'];} ?>" required>

            </select>
            <div class="invalid-feedback">
              Veuillez saisir une ville.
            </div>
          </div>
        </div>
          <div class="col-md-6 mb-3">
            <label for="code_postal">Code Postal</label>
            <input type="text" class="form-control" id="code_postal" name="code_postal" value="<?php if (isset($_POST["licence"]))
{ echo $x['code_postal'];} ?>" required>
            <div class="invalid-feedback">
              Veuillez saisir un code postal.
            </div>
          </div>
         
        <button class="btn btn-primary btn-lg btn-block" type="submit">Valider les informations</button>
       </form>
        </div>
    

         <!-- Onglet supprimer -->
        <div id="Supprimer" class="tab-pane fade">
          <form class="form-horizontal" action = "action_page.php">
            <div class="alert alert-secondary">
                <br>
                <p>Êtes-vous sûr de vouloir supprimer ce membre ?</p>
                <br>
                <div align=center><button type="button" class="btn btn-warning">Annuler</button>
                  <button type="button" class="btn btn-danger">Valider</button></div>
</body>
<?php
include 'include/footer.php';
?>
</div>
</html>