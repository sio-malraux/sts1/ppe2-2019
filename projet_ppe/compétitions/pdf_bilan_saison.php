<?php
require '../fonctions/connexion_bdd.php';

session_start();


        $historique = $conn->query("SELECT competition.date AS date,competition.nom AS nom,inscription.note_global as note 
      FROM llj_kata.competition 
      INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
      INNER JOIN llj_kata.inscription ON inscription.num_kata = competition.num_kata 
      WHERE inscription.licence_m = " . $_SESSION['licence'] . " 
      GROUP BY date, competition.nom, inscription.note_global 
      HAVING date < NOW() order by date DESC;");
        $i=0;
        while($affiche = $historique->fetch()) {
            $tab[$i]['nom'] = $affiche['nom'];
            $tab[$i]['date'] = $affiche['date'];
            $tab[$i]['note'] = $affiche['note'];
            $i++;
        }

        $avenir = $conn->query("SELECT competition.date AS date,competition.nom AS nom 
      FROM llj_kata.competition 
      INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
      INNER JOIN llj_kata.inscription ON inscription.num_kata = competition.num_kata  
      WHERE inscription.licence_m = " . $_SESSION['licence'] . "
      GROUP BY date, competition.nom, inscription.note_global
      HAVING date > NOW() order by date DESC;");
$a=0;
while($affiche = $avenir->fetch()) {
    $tab2[$a]['nom'] = $affiche['nom'];
    $tab2[$a]['date'] = $affiche['date'];
    $a++;
}



require __DIR__.'/../vendor/autoload.php';
   use Spipu\Html2Pdf\Html2Pdf;

ob_start();
?>

<page>
     <h1 align="center">Compétition</h1>
    <br>
    <h2 align="center">Historique</h2>
    <table bgcolor="#DCDCDC" border="0.2" align="center" class="table table-striped table-sm" >

        <thead>

        <tr>
            <th style="vertical-align:middle;" width="200" height="20" align="center" >Nom competition</th>
            <th style="vertical-align:middle;" width="200" height="20" align="center" >Date</th>
            <th style="vertical-align:middle;" width="200" height="20" align="center" >Note globale</th>

        </tr>
        </thead>
        <tbody>

        <?php
           $j=0;
            while($j < $i){
                ?>
                <tr>
                    <td style="vertical-align:middle;" width="200" height="20" align="center" ><?php echo $tab[$j]['nom']?></td>
                    <td style="vertical-align:middle;" width="200" height="20" align="center" ><?php echo $tab[$j]['date']?></td>
                    <td style="vertical-align:middle;" width="200" height="20" align="center" ><?php echo $tab[$j]['note']?></td>
                </tr>
            <?php $j++;
            }

         ?>
        </tbody>
    </table><br><br>
    <h2 align="center">A venir</h2>
    <table bgcolor="#DCDCDC" border="0.2" align="center" class="table table-striped table-sm" >

        <thead>

        <tr>
            <th style="vertical-align:middle;" width="200" height="20" align="center" >Nom competition</th>
            <th style="vertical-align:middle;" width="200" height="20" align="center" >Date</th>

        </tr>
        </thead>
        <tbody>

        <?php
        $j=0;
        while($j < $a){
            ?>
            <tr>
                <td style="vertical-align:middle;" width="200" height="20" align="center" ><?php echo $tab2[$j]['nom']?></td>
                <td style="vertical-align:middle;" width="200" height="20" align="center" ><?php echo $tab2[$j]['date']?></td>
            </tr>
            <?php $j++;
        }

        ?>
        </tbody>
    </table></page>
<?php
$content = ob_get_clean();
$html2pdf = new Html2Pdf();
$html2pdf->writeHTML($content);
$html2pdf->output();
?>