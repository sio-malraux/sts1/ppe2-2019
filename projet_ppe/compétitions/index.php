<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 02/03/2019
 * Time: 22:01
 */

include '../include/header.php';
include '../fonctions/fonctions.php';

date_default_timezone_set('UTC');
?>
<head>
    <link rel="stylesheet" href="../css/style.css">
    <script type="text/javascript" src="../js/podium.js"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<br>

<div class="container">

    <h1>Compétitions</h1>
    <a href="listes.php">
        <button class="btn btn-info my-2 my-sm-0" style="float: right; position: relative;bottom: 10px" type="submit">Toutes les compétitions</button>
    </a>
    <h5><i>Résultats des 3 dernières compétitions</i></h5>
<hr>
    <?php

    require_once '../fonctions/connexion_bdd.php';

   $competitions = $conn->query("SELECT num_kata, nom, date FROM llj_kata.competition GROUP BY num_kata, nom, date HAVING date < NOW() ORDER BY date DESC LIMIT 3");
    echo "<div class='card-deck'>";

    $dateDuJour = date("Y-m-d");
   while($donnees = $competitions->fetch()) {

       $resultat = $conn->query("SELECT membre.prenom, membre.nom FROM llj_kata.competition
INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata
INNER JOIN llj_kata.membre ON resultat.licence_m = membre.licence_m
WHERE competition.num_kata = ".$donnees['num_kata']."
ORDER BY resultat.note DESC LIMIT 1");
       $premier = $resultat->fetch();

       $resultatDeuxio = $conn->query("SELECT membre.prenom, membre.nom FROM llj_kata.competition
       INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata
       INNER JOIN llj_kata.membre ON resultat.licence_m = membre.licence_m
       WHERE competition.num_kata = ".$donnees['num_kata']."
       ORDER BY resultat.note DESC LIMIT 1 offset 1");
       $deuxieme = $resultatDeuxio->fetch();

       $resultatTrois = $conn->query("SELECT membre.prenom, membre.nom FROM llj_kata.competition
       INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata
       INNER JOIN llj_kata.membre ON resultat.licence_m = membre.licence_m
       WHERE competition.num_kata = ".$donnees['num_kata']."
       ORDER BY resultat.note DESC LIMIT 1 offset 2");
       $troisieme = $resultatTrois->fetch();


            //if($donnees['date'] < $dateDuJour == true) {

               ?>

               <div class="card">
                   <div class="card-body">
                       <form method="post" action="classement.php">
                           <h5 class="card-title"><strong><?php echo $donnees['nom']; ?></strong> -
                               #<?php echo $donnees['num_kata']; ?></h5>
                           <hr>
                           <p class="card-text">

                               La compétition était le <?php echo $donnees['date']; ?>.
                               <br>

                               <br>
                               Lieu: Le Mans, 72000, rue

                               <br>
                           </p>
                           <div class="competition-podium well">

                               <div class="podium-block bronze">
                                   <div class="name"><?php echo $troisieme['prenom']." ".$troisieme['nom'] ?></div>
                                   <div class="podium"><span>3<sup>ème</sup></span></div>
                               </div>
                               <div class="podium-block gold">
                                   <div class="name"><?php echo $premier['prenom']." ".$premier['nom'] ?></div>
                                   <div class="podium"><span>1<sup>er</sup></span></div>
                               </div>
                               <div class="podium-block silver">
                                   <div class="name"><?php echo $deuxieme['prenom']." ".$deuxieme['nom'] ?></div>
                                   <div class="podium"><span>2<sup>ème</sup></span></div>
                               </div>

                           </div>
                           <br>
                           <br>
                           <hr>
                           <div class="col text-center">
                               <a href="classement.php?id=<?php echo $donnees['num_kata']; ?>">
                                   <button class="btn btn-info my-2 my-sm-0" name="num_kata" type="submit">Voir classement</button>
                               </a>
                           </div>
                       </form>



                   </div>
                   <div class="card-footer">
                       <small class="text-muted">Date: <?php echo $donnees['date']; ?></small>
                   </div>
               </div>
               <?php



    ;
    ?>


       <?php
   }
   $competitions->closeCursor();
    echo '</div>'
    ?>

<hr>
<h1>Compétitions à venir</h1>
<h5><i>Les compétitions à venir dans les prochains mois/jour...</i></h5>
    <?php

        $competitionsAVenir = $conn->query("SELECT * FROM llj_kata.competition ORDER BY date ASC");
        $retour =-1;

        while ($donneesCompet = $competitionsAVenir->fetch()){

            $retour = $retour +1;

            if($retour == 0){
                echo "<div class='card-deck'>";
            }

            if(($donneesCompet['date'] > $dateDuJour) == true){

                ?>

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><strong><?php echo $donneesCompet['nom']; ?></strong> -
                            #<?php echo $donneesCompet['num_kata']; ?></h5>
                        <hr>
                        <p class="card-text">

                            La compétition commencera le <?php echo $donneesCompet['date']; ?>.
                            <br>

                            <br>
                            Lieu: Le Mans, 72000, rue

                            <br>
                        </p>
                        <hr>
                        <div class="col text-center">
                            <a href="participants.php?id=<?php echo $donneesCompet['num_kata']; ?>">
                                <button class="btn btn-info my-2 my-sm-0" type="button">Voir participants</button>
                            </a>

                        </div>


                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Date: <?php echo $donneesCompet['date']; ?></small>
                    </div>
                </div>

    <?php

            }
            if($retour == 2){
                $retour = -1;
                echo "</div><br><hr><br>";
            }
        }

    ?>
</div>
