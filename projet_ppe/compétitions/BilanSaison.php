<!DOCTYPE html>

<?php
   include '../include/header.php';
   require '../fonctions/connexion_bdd.php';
   require '../fonctions/fonctions.php';
   //echo $_SESSION['licence'];
   ?>

<!-- Titre -->
<div class="container">
  <h2  align="center">Bilan de la Saison</h2>
  <div class="panel panel-default">
    
    <!--Panel Nom candidat -->
    <div class="card-header">
      <h3>Nom candidat</h3>
      <div align="center" class="tab-content">
        <form  action="BilanSaison.php" method="POST"<br>
               <label for="name">Nom et prénom du membre :</label>
               
               <?php       
               $req = $conn->query("SELECT licence_m, nom, prenom from llj_kata.membre;");
               $requete = $conn->query("SELECT nom, prenom from llj_kata.membre where licence_m=;");
               ?>
               <select name="membre" id ="membre">
                   <?php
                    
                     while ($res = $req->fetch())
                     {
                         if (isset($_POST['membre']) && $_POST['membre']==$res['licence_m']) {
                       echo '<option value="'.$res['licence_m'].'" selected >'.$res['prenom'].' '.$res['nom'].'</option>';
                         }
                         else
                         {
                             echo '<option value="'.$res['licence_m'].'" >'.$res['prenom'].' '.$res['nom'].'</option>';
                         
                         }
                     }
                   ?>
                   
               </select>
              <!-- bouton valider -->
               <div align="center" class="form-group">
                 <div class="col-sm-offset-2 col-sm-10">              
                   <input type="submit" class="btn btn-success" name="boutonOK" value="Valider">
                 </div> 
               </div>



      </form>
      </div>
</div>

      <!-- bouton pdf-->
    <form name="pdf" action="pdf_bilan_saison.php" method="post">
        <div align="center" class="form-group">
        <input type="submit" class="btn btn-success" value="pdf">
    </form>

      <!-- bouton mail -->
    <form action="mail_bilan_saison.php"  method="post">
        <div align="center" class="form-group">
        <input name="mail" class="btn btn-success" type="submit" value="mail" />
    </form>
<div class="card-body">  

<!-- Affectation des requêtes dans des variables en fonction d'une personne connecter ou non -->
<?php
       if(isset($_SESSION['auth'])){ // si quelqu'un co

         if(isset($_POST['boutonOK'])){
       $historique = $conn->query("SELECT competition.date AS date,competition.nom AS nom,inscription.note_global as note 
      FROM llj_kata.competition 
      INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
      INNER JOIN llj_kata.inscription ON inscription.num_kata = competition.num_kata 
      WHERE inscription.licence_m = ".$_SESSION['licence']." 
      GROUP BY date, competition.nom, inscription.note_global 
      HAVING date < NOW() order by date DESC;"); 
       
       $avenir = $conn->query("SELECT competition.date AS date,competition.nom AS nom,inscription.note_global as note 
      FROM llj_kata.competition 
      INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
      INNER JOIN llj_kata.inscription ON inscription.num_kata = competition.num_kata  
      WHERE inscription.licence_m = ".$_SESSION['licence']."
      GROUP BY date, competition.nom, inscription.note_global
      HAVING date > NOW() order by date DESC;");
   }
       }

   if(!isset($_SESSION['auth'])){ // si personne non co
                 if(isset($_POST['boutonOK']))
             {

      $historique2 = $conn->query("SELECT competition.date AS date,competition.nom AS nom,inscription.note_global as note 
      FROM llj_kata.competition 
      INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
      INNER JOIN llj_kata.inscription ON inscription.num_kata = competition.num_kata 
      WHERE inscription.licence_m = ".$_POST['membre']."
      GROUP BY date, competition.nom, inscription.note_global
      HAVING date < NOW() order by date DESC;");
             
       $avenir2 = $conn->query("SELECT competition.date AS date,competition.nom AS nom,inscription.note_global as note 
      FROM llj_kata.competition 
      INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
      INNER JOIN llj_kata.inscription ON inscription.num_kata = competition.num_kata 
      WHERE inscription.licence_m = ".$_POST['membre']."
      GROUP BY date, competition.nom, inscription.note_global
      HAVING date > NOW() order by date DESC;");

         }
   }
         ?>


<!--Panel Historique -->
<h3>Historique</h3>
<div class="table-responsive">
  <table class="table table-striped table-sm">                
    
    <thead>
      
      <tr>
        <th>Nom competition</th>
        <th>Date</th>
        
        <th>Note globale</th>
        
      </tr>
    </thead>
    <tbody>
      
      <?php         
        if(isset($_SESSION['auth'])&& isset($_POST['boutonOK'])) // si il est connecté
             {                  
        while($affiche = $historique->fetch()){
            ?>          
            <tr>
            <td><option><?php echo $affiche['nom']?></option></td>
            <td><option><?php echo $affiche['date']?></option></td>
            <td><option><?php echo $affiche['note']?></option></td>
          </tr>
        <?php } }
        
        else if(!isset($_SESSION['auth']) && isset($_POST['boutonOK'])){ 
                while($affiche = $historique2->fetch()){
          ?>
          <tr>
            <td><option><?php echo $affiche['nom']?></option></td>
            <td><option><?php echo $affiche['date']?></option></td>
            <td><option><?php echo $affiche['note']?></option></td>
          </tr>
        <?php }} ?>
    </tbody>
  </table>
</div>

</div>
    
    
    <!--Panel A venir -->
<div class="card-footer">
  <h3>A venir</h3>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>Nom competition</th>
          <th>Date</th>
        </tr>
      </thead>
     
      <tbody> 
     <?php         
        if(isset($_SESSION['auth']) &&  isset($_POST['boutonOK'])){                  
        while($aff = $avenir->fetch()){
            ?>          
            <tr>
            <td><option><?php echo $aff['nom']?></option></td>
            <td><option><?php echo $aff['date']?></option></td>
          </tr>
        <?php } }
        
        else if(!isset($_SESSION['auth'])&& isset($_POST['boutonOK'])){ 
                while($aff = $avenir2->fetch()){
          ?>
          <tr>
            <td><option><?php echo $aff['nom']?></option></td>
            <td><option><?php echo $aff['date']?></option></td>            
          </tr>
   <?php }}?>
      </tbody>
    </table>
  </div>
</div>

</div>
</div>


</body>
</html>
