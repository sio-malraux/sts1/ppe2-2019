<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 03/03/2019
 * Time: 15:09
 */


include '../include/header.php';
require_once '../fonctions/connexion_bdd.php';
$compet_id = $_GET['id'];
//$classement = $conn->query("SELECT * FROM llj_kata.competition WHERE num_kata=".$_POST['num_kata']);
?>
<br>
<div class="container">

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Informations</h5>
                    <p class="card-text">Résultat de la compétition, les particpants sont triés en fonction de leur note global.</p>
                    <a href="index.php" class="btn btn-danger">Retour en arrière</a>
                </div>
            </div>
        </div>
        <?php
           ?>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Jury</h5>
                    <?php
                    $req = $conn->query("SELECT entraineur.nom, entraineur.prenom FROM llj_kata.jury 
INNER JOIN llj_kata.competition ON jury.num_kata = competition.num_kata INNER JOIN llj_kata.entraineur ON entraineur.licence_pro = jury.licence_pro
WHERE jury.num_kata=".$compet_id);

                    ?>
                    <?php while($recup = $req->fetch() ){
                        echo '<p class="card-text">';
                        echo "- ".$recup['prenom']." ".$recup['nom']."</p>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <br>


    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Placements</th>
            <th scope="col">Club</th>
            <th scope="col">Prénom</th>
            <th scope="col">Nom</th>
            <th scope="col">Note</th>
        </tr>
        </thead>
        <tbody>
        <?php


        $resultat = $conn->query("SELECT club.nom AS club, membre.prenom, membre.nom, resultat.licence_m, note FROM llj_kata.competition 
INNER JOIN llj_kata.resultat ON competition.num_kata = resultat.num_kata 
INNER JOIN llj_kata.membre ON resultat.licence_m = membre.licence_m
INNER JOIN llj_kata.club ON membre.num_club = club.num
WHERE competition.num_kata=".$compet_id."
ORDER BY note DESC");

        $placement = 1;

        while($donnees = $resultat->fetch()) {


            ?>

            <tr <?php if($placement == 1){ echo "class='bg-warning'"; } ?>>
                <th scope="row"><?php echo $placement; ?></th>
                <td><?php echo $donnees['club']; ?></td>
                <td><?php echo $donnees['prenom']; ?></td>
                <td><?php echo $donnees['nom']; ?></td>
                <td><?php echo $donnees['note']."/30"; ?></td>
            </tr>
            <?php
            $placement = $placement +1;
        }
        ?>
        </tbody>
    </table>
</div>


