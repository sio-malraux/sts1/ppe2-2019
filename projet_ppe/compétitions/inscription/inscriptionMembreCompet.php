<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <title>Accueil - Ligue Judoka</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

            <div class="collapse navbar-collapse" id="navb">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="javascript:void(0)">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://www.bts-malraux72.net/~q.pipelier/projet_ppe/compétitions/">Compétitions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Clubs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Sponsors</a>
                    </li>
                </ul>

                <div class="offset-4">

                    <?php
                    if (session_status() == PHP_SESSION_NONE) {
                        session_start();
                    }

                    if (isset($_SESSION['auth'])) {

                        echo '<a href="http://www.bts-malraux72.net/~q.pipelier/projet_ppe/compte/parametre.php">
          <button  class="btn btn-info my-2 my-sm-0" type="button">' . $_SESSION['pseudo'] . '</button></a>';

                        echo '<a href="http://www.bts-malraux72.net/~q.pipelier/projet_ppe/fonctions/logout.php">
          <button  class="btn btn-danger my-2 my-sm-0" type="button"> Déconnexion </button></a>';
                    } else {
                        echo '<a href="connexion.php">
          <button  class="btn btn-success my-2 my-sm-0" type="button">Connexion</button></a>';
                    }
                    ?>
                </div>
            </div>
        </nav>
        <?php
// Informations de connexions à la BDD
// 'pgsql:host=lienDuHost;port=numeroport;dbname=nom_database;user=utilisateur;password=mot_de_passe';
// DSN = Data Source Name
        $dsn = "pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=ppe2;user=ppe2;password=P@ssword";

        $conn = new PDO($dsn);
        ?>
        <!DOCTYPE html>



    <div class="container">
        <br> <h2>Formulaire Compétition</h2>

        <?php
            if (isset($_GET['message']))
            {
                echo ' REQUETE BIEN ENVOYEE';
            }
            ?>
        <div class="tab-content">
            <div id="Inscription" class="tab-pane fade show active" >

                <br><h3>Inscription/Suppression d'un membre à une compétition</h3>

                <hr>

                

                    <h3><u>Compétition</u></h3>

                    <form class="form-horizontal" action = "inscriptionMembreCompet.php" method='POST'>

                        <br><SELECT id='competition' name="competition" size="1">
                            <?php
                            $requete = "SELECT nom,num_kata,annee FROM llj_kata.competition";
                            $resultats = $conn->query($requete);

                            while ($donnees = $resultats->fetch()) {
                                if (isset($_POST['licence'])) {  // si on a cliqué sur Rechercher il faut garder en liste la compet sélectionnée
                                    ?>
                                    <option value="<?php echo $donnees['num_kata'].'/'.$donnees['annee']; ?>" 
                                            <?php if ($_POST['competition'] == $donnees['num_kata']) echo 'selected'; ?>>
                                                <?php echo $donnees['nom'] . ", en " . $donnees['annee']; ?>
                                    </option>
                                    <?php
                                }// fin if
                                else { // affichage normal de la liste
                                    ?>
                                    <option value="<?php echo $donnees['num_kata'].'/'.$donnees['annee']; ?>">
                                        <?php echo $donnees['nom'] . ", en " . $donnees['annee'] ?>
                                    </option>
                                    <?php
                                }// fin else
                            } // fin while
                            ?> 
                        </SELECT>

                        <hr>

                        <h3 class="Titre"><u>Membre</u></h3>
                       <!-- <form class="needs-validation" method="POST" action="inscriptionMembreCompet.php">  -->



                            <div class="col-md-3 mb-3">
                                <label for="licence">N° Licence :</label>

                                <select class="custom-select d-block w-100" id="licence"  name="licence"  required>
                                    <option value="" > </option>

                                    <?php
// ajout des num licence trouvés en bdd dns la liste (SELECT)
                                    $requeteLicence = "SELECT licence_m FROM llj_kata.membre ORDER BY licence_m";
                                    $resultatLicence = $conn->query($requeteLicence);

                                    while ($numLicence = $resultatLicence->fetch()) {
                                        if (isset($_POST['licence'])) {  // si on a cliqué sur Recherche il faut garder visible en liste le num licence sélectionné
                                            ?>
                                            <option value="<?php echo $numLicence['licence_m']; ?>" 
                                                    <?php if ($_POST['licence'] == $numLicence['licence_m']) echo 'selected'; ?> >
                                                        <?php echo $numLicence['licence_m']; ?>
                                            </option>

                                            <?php
                                        } // fin if
                                        else { // affichage normal de la liste
                                            ?>
                                            <option value="<?php echo $numLicence['licence_m']; ?>"><?php echo $numLicence['licence_m']; ?></option>
                                            <?php
                                        }// fin else
                                    } // fin while
                                    ?> 
                                </select>

                                <div class="col-6">
                                   <!-- <input type="text" id='numLicence' name="numLicence" class="form-control" placeholder="Numéro de licence" required> -->
                                    <input name="rechercher" type="submit"  value="Rechercher"/>

                                </div>
                        </form>


                        <?php
                        if (isset($_POST['licence'])) {
//$requetemembre="SELECT * FROM llj_kata.membre WHERE licence_m=".$donneesmembre['numLicence'].";";
                            $requetemembre = "SELECT licence_m, nom, prenom, genre, e_mail, tel FROM llj_kata.membre WHERE licence_m= " . $_POST['licence'] . " ;";
                            $resultatsmembre = $conn->query($requetemembre);
                            $_SESSION['competition']= $_POST['competition'];
                             $_SESSION['licence']= $_POST['licence'];
                        }
                        ?>



                        <br> <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Prénom</th>
                                    <th scope="col">Genre</th>

                                    <th scope="col">Mail</th>
                                    <th scope="col">Tel.</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($_POST['licence'])) {
                                    while ($donneesmembre = $resultatsmembre->fetch()) {
                                        ?>


                                    <th scope="row"><?php echo $donneesmembre['licence_m'] ?></th>
                                    <td><?php echo $donneesmembre['nom'] ?></td>
                                    <td><?php echo $donneesmembre['prenom'] ?></td>
                                    <td><?php echo $donneesmembre['genre'] ?></td>
                                    <td><?php echo $donneesmembre['e_mail'] ?></td> <!-- à récupérer pour le mail-->
                                    <td><?php echo $donneesmembre['tel'] ?></td>




                                    <?php
                                }
                            }
                            ?>

                            </tbody>
                        </table>


                        <center>
                            <br>
                            <table>
                                <tr><td>
                            <form action="membreInscrit.php" method="POST">

                                <input class="btn btn-primary" type="submit" value="Inscrire">
                            </form></td>
                                <td>
                                <form action="membreSupprime.php" method="POST">
                                    <input  class="btn btn-danger" type="submit" value="Supprimer">
                                </form>
                                </td></tr>
                            </table>
                        </center>


                    </form>
                    


            </div>
        </div>
    </div>

    <script type="text/javascript">

    </script>

</body>
</html>