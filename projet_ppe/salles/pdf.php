<?php
session_start();
require __DIR__.'/../vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;


  ob_start();
  ?>
<page>Vous avez réserver la salle "<?php echo $_SESSION['nom_salle']; ?>" le <?php echo $_SESSION['date']; ?> à <?php echo $_SESSION['heure']; ?> pour une durée de <?php echo $_SESSION['duree']; ?>. </page>
      <?php
    $content = ob_get_clean();
$html2pdf = new Html2Pdf();
$html2pdf->writeHTML($content);
$html2pdf->output('reservation.pdf');

?>

    