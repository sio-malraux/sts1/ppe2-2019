<?php

include '../include/header.php';
include '../fonctions/connexion_bdd.php';
      
    $anneep=$_POST['annee']-1;
    $annees=$_POST['annee']+1;
    $requete="select nom, capacité, date_reservation, heure_reservation from llj_kata.reservation inner join llj_kata.salle on reservation.code_salle = salle.code where date_reservation > '$anneep-12-31' and date_reservation < '$annees-01-01' order by date_reservation desc;";
    $resultat=$conn->query($requete);
    
?>
  
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>Nom Salle</th>
              <th>Capacité</th>
              <th>Date de réservation</th>
              <th>Heure de réservation</th>
            </tr>
          </thead>
          <tbody>
              <?php
while ($ligne = $resultat->fetch())
  {
    echo '<tr>';
    echo '<td>' . $ligne['nom'] . '</td>';
    echo '<td>' . $ligne['capacité'] . '</td>';
    echo '<td>' . $ligne['date_reservation'] . '</td>';
    echo '<td>' . $ligne['heure_reservation'] . '</td>';
    echo '</tr>';
  }
?>
         </tbody>
        </table>
      </div>

<div class="form-group"> 
  <div class="col-sm-offset-2 col-sm-10"> 
    <form>
      <input type="button" value="Retour" class="btn btn-success" onclick="history.back()">
    </form>
  </div>
</div>