<?php
include '../fonctions/fonctions.php';
admin_only();
include '../include/header.php';
include '../fonctions/connexion_bdd.php';

?>

<html>
<h1 align="center">Gestion et réservation des salles</h1>
<div class="card-header">
<ul class="nav nav-pills">
  <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#gestions">Gestion des salles</a></li>
  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#gestionr">Gestion des réservations</a></li>  
  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ajoutr">Ajout d'une réservation</a></li>
  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ajouts">Ajout d'une salle</a></li>
  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#modifier">Modification d'une salle</a></li>
  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#supprimer">Suppression d'une salle</a></li>
</ul>
    
  
 
    <?php
$requete1  = "SELECT * FROM llj_kata.salle;";
$resultat1 = $conn->query($requete1);
?>
   
     <?php
$requete2  = "SELECT nom, capacité FROM llj_kata.salle;";
$resultat2 = $conn->query($requete2);
?>
   
<div class="tab-content">
  <!-- Onglet gestion des salles -->
  <div id="gestions" class="tab-pane fade show active">
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>Code</th>
              <th>Nom Salle</th>
              <th>Lieu</th>
              <th>Capacité</th>
            </tr>
          </thead>
          <tbody>
              <?php
while ($ligne = $resultat1->fetch())
  {
    echo '<tr>';
    echo '<td>' . $ligne['code'] . '</td>';
    echo '<td>' . $ligne['nom'] . '</td>';
    echo '<td>' . $ligne['situation'] . '</td>';
    echo '<td>' . $ligne['capacité'] . '</td>';
    echo '</tr>';
  }
?>
         </tbody>
        </table>
      </div>    

  </div>
  <!-- Onglet ajout d'une salle-->
  <div id="ajouts" class="tab-pane fade">
     <form class="form-horizontal" action = "pagea.php" method="post">
            <div class="alert alert-secondary">
              <p> Entrez le nom de la salle : 
                <input id="nom_salle" name="nom_salle" placeholder="prénom NOM" type="text" required /> </br></br> 
              </p>
              <p> Entrez le lieu de la salle : 
                <input id="lieu_salle" name="lieu_salle" placeholder="1er etage - batiment 1" type="text" required /> </br></br> 
              </p>
              <p> Entrez la capacité de la salle : 
                <input id="capacite_salle" name="capacite_salle" type="number" min="1" value="10" required /> </br></br> 
              </p>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <button type="submit" class="btn btn-success">Valider</button> 
                </div> 
              </div>
            </div>
          </form>

  </div>
  <!-- Onglet modification d'une salle -->
  <div id="modifier" class="tab-pane fade">
    <form class="form-horizontal" action = "pagem.php" method="post">
            <div class="alert alert-secondary">
              <p> Quelle salle voulez-vous modifier :    
                <select name="list_competition">
                  <?php
$sallem = $conn->query("SELECT nom FROM llj_kata.salle;");
while ($mod = $sallem->fetch())
  {
    echo '<option>' . $mod['nom'] . '</option>';
  }
?>
               </select>      
              </p>
              <p> Entrez le nom de la salle : 
                <input id="nom_salle" name="nom_salle" placeholder="prénom NOM" type="text" required /> </br></br> 
              </p>
              <p> Entrez le lieu de la salle : 
                <input id="lieu_salle" name="lieu_salle" placeholder="1er etage - batiment 1" type="text" required /> </br></br> 
              </p>
              <p> Entrez le nombre de personne maximum que peut contenir la salle : 
                <input id="capacite_salle" name="capacite_salle" placeholder="10" type="text" required /> </br></br> 
              </p>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <button type="submit" class="btn btn-success">Valider</button> 
                </div> 
              </div>
            </div>
          </form>
  </div>
  
  <!-- Onglet suppression d'une salle -->
  <div id="supprimer" class="tab-pane fade">
    <form class="form-horizontal" action = "pages.php" method="post">
            <div class="alert alert-secondary">
              <p> Quelle salle voulez-vous supprimer :    
                  <select name="list_competition">
                  <?php
$salles = $conn->query("SELECT nom FROM llj_kata.salle;");
while ($sup = $salles->fetch())
  {
    echo '<option>' . $sup['nom'] . '</option>';
  }
?>
               </select>      
              </p> 
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <button type="submit" class="btn btn-success">Valider</button> 
                </div> 
              </div>
            </div>
          </form>
  </div>
  
  <!-- Onglet ajout d'une réservation -->
  <div id="ajoutr" class="tab-pane fade">
    <form class="form-horizontal" action = "pager.php" method="post">
            <div class="alert alert-secondary">
               <div class="row">
          <div class="col-md-4 mb-3">
            <label for="date">Date de réservation</label> 
            <input type="date" class="form-control" name="date" id="date" required />
            <div class="invalid-feedback">
              Valid date is required.
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="heure">Heure de réservation</label>
            <input type="time" class="form-control" name="heure" id="heure" required />
            <div class="invalid-feedback">
              Valid date is required.
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="duree">Durée de réservation</label>
            <input type="time" class="form-control" name="duree" id="duree" required />
            <div class="invalid-feedback">
              Valid duree is required.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="code">Salle</label>
            <select class="custom-select d-block w-100" name="code" id="code" required>
                <?php
while ($ligne = $resultat2->fetch())
  {
    echo '<option>' . $ligne['nom'] . '</option>';
  }
?>
           </select>
            <div class="invalid-feedback">
              Please select a valid room.
            </div>
          </div>
        </div>
                
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <button type="submit" class="btn btn-success">Valider</button> 
                </div> 
              </div>
            </div>
          </form>
  </div>
  
  <!-- Onglet gestion des réservations -->
  <div id="gestionr" class="tab-pane fade">
    <form class="form-horizontal" action = "pageg.php" method="post">
            <div class="alert alert-secondary">
              <p> Année de réservation : 
                <input id="annee" name="annee" type="number" value="2019" size="4" min="1980" required /> </br></br> 
                <div class="invalid-feedback">
              Valid annee is required.
              </div>
              </p>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <button type="submit" class="btn btn-success">Valider</button> 
                </div> 
              </div>
            </div>
          </form>
  </div>
 
</div>
</div>
</html
