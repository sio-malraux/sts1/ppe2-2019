<!DOCTYPE html>

<?php

   session_start();
   include '../include/header.php';
   include '../fonctions/connexion_bdd.php';
  
   ?>

<div class="container">
  
  <div class="panel panel-default">
      <div class="card-header">
          </br> <h2 align="center">CHOIX DU MATÉRIEL<?php $_SESSION['kata_mat']=$_POST['ChoixCompetition']; ?></h2>
          </div>
    <div class="card-footer">
              
        <br /><br />
        
        <div class="alert alert-secondary">
 
            <form method='post' action='GestionMaterielRecap.php'> 
                
                <?php $req="select * from llj_kata.materiel";
                
                $res=$conn->query($req);
                while ($mat=$res->fetch()){
                    
                
                    echo '<p><h4 align="center" > '.$mat["libelle"] .' : '; 
        
          echo '<input name="'.$mat["num_materiel"].'" type="number" min="0" max="999" step="1" value="0" size="0"></p>';
                } ?>
           
 
            
          </div>
        
              <!-- Bouton Valider -->
              <div class="form-group"> 
                <div align="center" > 
                  <button type="submit" class="btn btn-success">Valider</button> 
                </div> 
              </div>

</form>

      </div>
      
    </div>
  </div>
</div>

</body>
</html>

