<?php

include '../include/header.php';
include '../fonctions/fonctions.php';

ini_set('display_errors','off');
?>
<!-- Changement de couleur de fond en fonction de l'heure -->
<style type="text/css" filter>
    body {
        background-color:
    <?php
    $heure = date('H');
    if($heure >= 9 AND $heure <=18)
    {
        echo'#FDFEFE';
    }
    else
    {
        echo '#5F5E70';
    }
    ?>
    ;
    }
</style>


<head>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    
</head>
<div class="container">
    <br>
    <br>
<hr>
    <h1 align="center" style="font-family: 'Times New Roman'"><b>Inscription d'un club</b></h1>
<hr>
    <br>
    <br>
    <br>


    <div class="card-header">
        <ul class="nav nav-pills">

            <li class="nav-item"><a class="nav-link <?php
                if($_SESSION['club_modification'] != 1){
                    echo "active";
                }

                ?>" data-toggle="tab" href="#gestion">Gestion Club</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Ajouter">Ajouter</a></li>
            <li class="nav-item"><a class="nav-link <?php
                if($_SESSION['club_modification'] == 1){
                    echo "active";
                }

                ?>" data-toggle="tab" href="#Modifier">Modifier</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Supprimer">Supprimer</a></li>
        </ul>

    </div>



    <div class="tab-content">
        <br>
        <!-- Onglet Gestion -->
        <div id="gestion" class="tab-pane fade <?php
        if($_SESSION['club_modification'] != 1){
            echo "show active";
        }

        ?> ">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Club</th>
                        <th>President</th>
                        <th>Adresse</th>
                        <th>Ville</th>
                        <th>Telephone</th>
                        <th>Mail</th>
                    </tr>
                    </thead>
                    <tbody bgcolor="beige">
                    <?php
                    require_once '../fonctions/connexion_bdd.php';

                    $resultat = $conn->query("SELECT * FROM llj_kata.club ORDER BY num DESC");

                    while($donnee = $resultat->fetch())
                    {


                        ?>

                        <tr>
                            <th scope="row"><?php echo $donnee['nom']; ?></th>
                            <td><?php echo $donnee['nom_president']; ?></td>
                            <td><?php echo $donnee['rue']; ?></td>
                            <td><?php echo $donnee['ville']; ?></td>
                            <td><?php echo $donnee['tel']; ?></td>
                            <td><?php echo $donnee['mail']; ?></td>

                        </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>

                <script>
                    $(document).ready(function() {
                        $('#example').DataTable( {
                            "language": {
                                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                            },
                            "order": [[ 3, "desc" ]]
                        } );
                    } );
                </script>
            </div>
        <br>
        <br>
        <!-- Onglet Valider pour envoie d'un email -->

        <form class="form-horizontal" method="post" action = "EnvoiMail.php">
            <div class="alert alert-secondary" align="center">
                <p> Choisir le club à contacter :
                    <select id="num_club" name="num_club">
                        <?php

                        $ValidMail = $conn->query("SELECT * FROM llj_kata.club");

                        while($listesClubs = $ValidMail->fetch()){


                            ?>
                            <option value=<?php echo "".$listesClubs['num'] ?>> <?php echo $listesClubs['nom'];?></option>

                            <?php

                        }

                        ?>
                    </select>
                </p>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
                    </div>
                </div>
            </div>
        </form>

            <form class="form-horizontal" method="post" action = "test.php">
                <div class="alert alert-secondary" align="center">
                    <p> Résumé des informations de clubs :</p>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="valider" values="valider" class="btn btn-success">PDF</button>
                        </div>
                    </div>
                </div>
            </form>




        </div>

        <!-- Onglet Saisir -->
        <div id="Ajouter" class="tab-pane fade">
            <form class="form-horizontal" method="post" action = "insertion.php">
                <div class="alert alert-secondary">
                    <p> Entrez le nom club :
                        <input id="nom" name="nom" placeholder="Kamatoe" type="text" required /> </br></br>
                    </p>

                    <p> Entrez le nom du President du club :
                        <input id="president" name="president" placeholder="Dupuy" type="text" required /> </br></br>
                    </p>

                    <p> Entrez l'adresse du club :
                        <input id="adresse" name="adresse" placeholder="15 rue Coquino" type="text" required /> </br></br>
                    </p>

                    <p> Entrez la ville du club :
                        <input id="ville" name="ville" placeholder="Allonnes" type="text" required /> </br></br>
                    </p>

                    <p> Entrez le téléphone du club :
                        <input id="tel" name="tel" placeholder="02 ** ** ** **" type="text" required /> </br></br>
                    </p>

                    <p> Entrez le mail du club :
                        <input id="mail" name="mail" placeholder="azerty@orange.fr" type="text" required /> </br></br>
                    </p>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Onglet modifier -->
        <div id="Modifier" class="tab-pane fade <?php
        if($_SESSION['club_modification'] == 1){
            echo "show active";
        }

        ?>" >
            <form class="form-horizontal" method="post" action = "tempo.php" >

                <div class="alert alert-secondary">
                    <p align="center"> Choisir le club a modifier :
                        <select id="nom_club" name="nom_club">

                            <?php

                            $clubs = $conn->query("SELECT * FROM llj_kata.club");

                            while($listesClubs = $clubs->fetch()){


                                ?>
                                <option value=<?php echo "".$listesClubs['num'] ?>> <?php echo $listesClubs['nom'];?></option>

                                <?php

                            }
                            ?>


                        </select>
                        <button type="submit" class="btn btn-info">OK</button>
                    </p>
            </form>
            <hr>
            <br>

            <form class="form-horizontal" method="post" action = "modification.php">
                <?php


                $modif_requete = $conn->query("SELECT club.nom AS nom, club.nom_president AS president, club.rue AS adresse, club.ville AS ville, club.tel AS tel, club.mail AS mail FROM llj_kata.club WHERE num=".$_SESSION['club_a_modif'].";");


                $donneClub = $modif_requete->fetch();
                if(isset($_SESSION['club_modification']) && $_SESSION['club_modification'] == 1){

                    ?>
                    <p> Modifier le nom club :
                        <input id="nom" name="nom" value="<?php echo $donneClub['nom'] ?>" type="text" required /> </br></br>
                    </p>

                    <p> Modifier le nom du President du club :
                        <input id="president" name="president" value="<?php echo $donneClub['president'] ?>" type="text" required  /> </br></br>
                    </p>

                    <p> Modifier l'adresse du club :
                        <input id="adresse" name="adresse" value="<?php echo $donneClub['adresse'] ?>" type="text" required /> </br></br>
                    </p>

                    <p> Modifier la ville du club :
                        <input id="ville" name="ville" value="<?php echo $donneClub['ville'] ?>" type="text" required /> </br></br>
                    </p>

                    <p> Modifier la telephone du club :
                        <input id="tel" name="tel" value="<?php echo $donneClub['tel'] ?>" type="text" required /> </br></br>
                    </p>

                    <p> Modifier le mail du club :
                        <input id="mail" name="mail" value="<?php echo $donneClub['mail'] ?>" type="text" required /> </br></br>
                    </p>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="modifier" values="modifier" class="btn btn-success">Modifier</button>
                            <button type="reset" name="rein" values="rein" class="btn btn-warning">Réintialisé</button>
                            <button type="submit" name="Annuler" values="Annuler" class="btn btn-danger">Annuler</button>

                        </div>
                    </div>
                    <?php

                }
                ?>

        </div>
        </form>
    </div>
    <!-- </div> -->

    <!-- Onglet supprimer -->
    <div id="Supprimer" class="tab-pane fade">
        <form class="form-horizontal" method="post" action = "suppression.php">
            <div class="alert alert-secondary" align="center">
                <p> Choisir le club a supprimer :
                    <select id="num_club" name="num_club">
                        <?php

                        $suppressionClub = $conn->query("SELECT * FROM llj_kata.club");

                        while($listesClubs = $suppressionClub->fetch()){


                            ?>
                            <option value=<?php echo "".$listesClubs['num'] ?>> <?php echo $listesClubs['nom'];?></option>

                            <?php

                        }

                        ?>

                    </select>
                </p>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="supprimer" values="supprimer" class="btn btn-danger">Supprimer</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
</div>
