<?php

include '../../include/header.php';
include '../../fonctions/connexion_bdd.php';

$user_id = $_GET['id'];
$req1 = $conn->query('SELECT club.nom FROM llj_kata.club');
$affiche = $req1->fetch();
?>
<body>
<div>
    <br>
    <br>
    <div class="container">

        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title" align="center"><b>Club sélectionné :</b></h1>
                        <h3 align="center"><?php  echo $affiche['nom'] ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title" align="center">Au revoir</h5>

                        <div class="form-group">
                            <div class="text-center submit-row" class="col-sm-offset-2 col-sm-10">
                                <a href="../" >
                                <button type="submit" name="retour" values="retour"  class="btn btn-danger">Retour à la page d'accueil</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>

    <div class="container">
        <div class="row">
            <div class="col-md-12"><table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Mail</th>
                        <th>Genre</th>
                        <th>Téléphone</th>
                    </tr>



                    </tbody>

                    <?php

                    $req = $conn->query('SELECT membre.nom AS nom, membre.prenom AS prenom, membre.e_mail AS mail, membre.genre AS genre, membre.tel AS tel FROM llj_kata.club
                        INNER JOIN llj_kata.membre ON membre.num_club = club.num WHERE club.num = '.$user_id);

                    while($affiche = $req->fetch()){
                        ?>

                        <tr>
                            <td><?php echo $affiche['nom']; ?></td>
                            <td><?php echo $affiche['prenom']; ?></td>
                            <td><?php echo $affiche['mail']; ?></td>
                            <td><?php
                                if($affiche['genre'] == 'H')
                                    echo 'Homme';
                                else
                                    echo 'Femme';

                                ?></td>
                            <td><?php echo $affiche['tel']; ?></td>

                        </tr>
                    <?php
                    }

                    ?>
                </table></div>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="assets/js/Dynamic-Table.js"></script>
</body>

</html>
