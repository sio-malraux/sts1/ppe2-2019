<?php

session_start();
/*
 * Générer un PDF à partir d'une base de données
 */
require '../fonctions/connexion_bdd.php';
include '../vendor/autoload.php';
//require_once dirname(__FILE__).'/../vendor/autoload.php';
    use Spipu\Html2Pdf\Html2Pdf;
    use Spipu\Html2Pdf\Exception\Html2PdfException;
    use Spipu\Html2Pdf\Exception\ExceptionFormatter;

ob_start();
?>

<page backtop="5%" backbottom="5%" backleft="5%" backright="5%">

    <?php

    $membre = $conn->query("SELECT club.nom AS nom, club.num AS num, club.rue AS rue, club.ville AS ville, club.tel AS tel, club.mail AS mail, club.nom_president AS nom_president FROM llj_kata.club ORDER BY club.num ASC ");
    //SELECT club.nom, club.num, club.rue, club.ville, club.nom, club.tel, club.mail, club.nom_president, count(membre.licence_m) AS licence FROM llj_kata.club INNER JOIN llj_kata.membre ON membre.num_club=club.num GROUP BY club.num ORDER BY club.num ASC ");



    ?>

    <div class ="container">

        <br>

        <h1>Les Clubs</h1>
        <h5><i>Tout les clubs existants</i></h5>
        <hr>
        <?php
        $retour_ligne= -1;

        while ($affiche = $membre->fetch()){

            $retour_ligne = $retour_ligne +1;

            if($retour_ligne == 0){
                echo '<div class="card-deck">';
            }

            $nbr_membre = $conn->query("SELECT count (*) AS nbr FROM llj_kata.club INNER JOIN llj_kata.membre ON membre.num_club=club.num WHERE club.nom='" . $affiche['nom'] . "'");
            $affichage_membre = $nbr_membre->fetch();
            ?>


            <div class="card">
                <div class="card-body">
                    <p style="color:#1E90FF;" align="center"><b><?php echo $affiche['nom']; ?></b></p>

                    <hr>
                    <p class="card-text"><U> Informations :</U>
                        <br>
                        - Nombre Membre : <?php  echo $affichage_membre['nbr'] ?>
                        <br>
                        - Adresse :

                        <br>


                        Ville : <?php echo $affiche['ville'];?>
                        <br>

                        Rue : <?php echo $affiche['rue'];?>
                    <hr>
                    <U> Contactez-nous : </u>
                    <br>

                    Tél : <?php echo $affiche['tel'];?>
                    <br>

                    Mail : <?php echo $affiche['mail'];?>
                    <hr>
                    <u> Dirigeant :</u> <?php echo $affiche['nom_president']; ?>
                    <hr background-color="blue">
                    </p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">  </small>
                </div>
            </div>
            <?php

            if($retour_ligne == 2){
                echo '</div>';
                echo '<br><hr><br>';
                $retour_ligne = -1;
            }
        }
        ?>

    </div>
</page>

<?php
$content = ob_get_clean();

$html2pdf = new Html2Pdf('P','A4', 'fr', 'true', 'UTF-8');
$html2pdf->writeHTML($content);
$html2pdf->output();

?>


<!-- Faire un onglet choix d'information sur un club (liste déroulante) avec résultat de la demande en PDF -->
