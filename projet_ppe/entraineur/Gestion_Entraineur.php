<?php
include '../include/header.php';
include '../fonctions/connexion_bdd.php';
?>
<br>
<br>
<h1 align="center">Gestion des Entraineurs</h1>
<br>
<br>
<br>
<div class="card-header">
    <ul class="nav nav-pills">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#gestion">Gestion Entraineur</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Ajouter">Ajouter</a></li>
        <li class="nav-itm"><a class="nav-link" data-toggle="tab" href="#Modifier">Modifier</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Supprimer">Supprimer</a></li>
    </ul>


    <div class="tab-content">

        <!-- Onglet Gestion -->
          <div id="gestion" class="tab-pane fade <?php
    if($_SESSION['club_modification'] != 1){
        echo "show active";
    }
    ?> ">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>licence pro</th>
                            <th>rang</th>
                            <th>num club</th>
                            <th>nom</th>
                            <th>prenom</th>
                            <th>genre</th>
                            <th>telephone</th>
                            <th>Mail</th>
                        </tr>
                       </thead>
                      <tbody>
<?php
$cocoletableau = $conn->query("SELECT * FROM llj_kata.entraineur ORDER BY licence_pro DESC");
while ($michel = $cocoletableau->fetch()){
    ?> 
               <tr>
                <td><?php echo '<option>' . $michel['licence_pro'] . '<option>' ?></td>
                <td><?php echo '<option>' . $michel['rang'] . '<option>' ?></td>  
                <td><?php echo '<option>' . $michel['num_club'] . '<option>' ?></td>  
                <td><?php echo '<option>' . $michel['nom'] . '<option>' ?></td>  
                <td><?php echo '<option>' . $michel['prenom'] . '<option>' ?></td>  
                <td><?php echo '<option>' . $michel['genre'] . '<option>' ?></td>  
                <td><?php echo '<option>' . $michel['tel'] . '<option>' ?></td>
                <td><?php echo '<option>' . $michel['e_mail'] . '<option>' ?></td> 
               </tr>
    <?php
}
?>

                    </tbody>
                </table>

            </div>

        </div>

        <!-- Onglet Saisir -->
        <div id="Ajouter" class="tab-pane fade">
            <form class="form-horizontal" method="post" action = "ajout_Entraineur.php">
                <div class="alert alert-secondary">

                    
                    <p> Entrez le Rang de l'entraineur : 
                        <input id="rang" name="rang" placeholder="3eme dan" type="text" required /> </br></br> 
                    </p>
                    
                    <p>  
                        le numéro de club : 
                        <select id="list_club" name="list_club">
                           <?php 
                           $requete12 = $conn->query("select num from llj_kata.club");
                           while($donnees = $requete12 ->fetch()){
                               ?>
                              <option value=<?php echo "".$donnees['num'] ?>> <?php echo $donnees['num'];?></option>
                           
                           <?php
                           }
                           ?>
                        </select>
                        
                    </p>

                    <p> Entrez le nom de l'entraineur : 
                        <input id="nom" name="nom" placeholder="Adler" type="text" required /> </br></br> 
                    </p>

                    <p> Entrez le prenom de l'entraineur :
                        <input id="prenom" name="prenom" placeholder="Karel" type="text" required /> </br></br> 

                    <p> Entrez le genre : 
                        <input id="genre" name="genre" placeholder="H" type="text" required /> </br></br> 
                    </p>

                    <p> Entrez le téléphone : 
                        <input id="tel" name="tel" placeholder="06 ** ** ** **" type="text" required /> </br></br> 
                    </p>
                    
                    <p> Entrez le Mail : 
                        <input id="mail" name="mail" placeholder="exemple@orange.fr" type="text" required /> </br></br> 
                    </p>

                    

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10"> 
                            <button type="submit" class="btn btn-success">Valider</button> 
                        </div> 
                    </div>
                </div>
            </form>
        </div>

        <!-- Onglet modifier -->
        <div id="Modifier" class="tab-pane fade"><?php
        if($_SESSION['num'] == 1){
            echo "show active";
        }
       
           ?>       
            <form class="form-horizontal" method="post" action="tempo.php">
                
                <div class="alert alert-secondary">
                    <p> Choisir l'entraineur a modifier :    
                       <select> id="list_entraineur name="list_entraineur">
                           <?php 
                           $requete1 = $conn->query("select licence_pro,nom,prenom from llj_kata.entraineur");
                           while($donnees = $requete1 ->fetch()){
                               ?>
                              <option value=<?php echo "".$donnees['licence_pro'] ?>> <?php echo $donnees['nom'];?><?php echo " ";?> <?php echo $donnees['prenom'];?></option>
                           
                           <?php
                           }
                           ?>
                           
                       </select> 
                        <button type="submit" class="btn btn-info">OK</button>
              </p>   </form>  
                <form class="form-horizontal" method="post" action = "Gestion_Entraineur.php" action="modif_Entraineur.php">
                        
                        
                           </p> 

                    <p> Modifier le nom club : 
                        <input id="Num_club" name="Num_club" placeholder="12" type="text" required /> </br></br>
                        
                    </p>
                    
                    <p> Modifier le rang : 
                        <input id="rang" name="rang" placeholder="Noir 1er DAN" type="text" required /> </br></br>
                        
                    </p>
                    
                    <p> Modifier le nom de l'entraineur : 
                        <input id="Nom_entraineur" name="Nom_entraineur" placeholder="Dupuy" type="text" required /> </br></br>
                    </p>

                    <p> Modifier le prenom de l'entraineur : 
                        <input id="prenom_entraineur" name="prenom_entraineur" placeholder="Marc" type="text" required /> </br></br> 
                    </p>
                    
                    <p> Modifier le genre de l'entraineur : 
                        <input id="genre" name="genre_entraineur" placeholder="H" type="text" required /> </br></br> 
                    </p>
                    

                    <p> Modifier la telephone de l'entraineur: 
                        <input id="tel_entraineur" name="tel_entraineur" placeholder="06 ** ** ** **" type="text" required /> </br></br> 
                    </p>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10"> 
                            <button type="submit" name="modifier" method = "post" values="modifier" action = "Gestion_Entraineur.php"  class="btn btn-warning" >Modifier</button> 
                            <button type="reset"  name="rein"    method = "post" values="rein" action = "Gestion_Entraineur.php" class="btn btn-warning">Réintialisé</button> 
                            <button type="submit" name="Annuler" method = "post" values="Annuler" action = "Gestion_Entraineur.php" class="btn btn-danger">Annuler</button> 

                        </div> 
                    </div>
                </div>
            </form>
        </div>

        <!-- Onglet supprimer -->
 <div id="Supprimer" class="tab-pane fade">
            <form class="form-horizontal" method="post" action = "supp_Entraineur.php">
            <div class="alert alert-secondary">
              <p> Choisir l'entraineur a supprimer :    
                <select id="num_entraineur" name="num_entraineur">
                   <?php
                    
                    $suppentraineur = $conn->query("SELECT * FROM llj_kata.entraineur");
                    
                    while($listesentraineur = $suppentraineur->fetch()){
                    
                    
                    ?>
                  <option value=<?php echo "".$listesentraineur['licence_pro'] ?>><?php echo "".$listesentraineur['prenom'] ?> <?php echo "".$listesentraineur['nom'] ?> <?php echo $listesentraineur['licence_pro'];?></option>
                  
                  <?php
                  
                    }
                    
                    ?>
                </select>      
              </p> 


              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10"> 
                  <button type="submit" name="supprimer" values="supprimer" class="btn btn-danger">Supprimer</button> 

                        </div> 
                    </div>
                </div>
            </form>
        </div>  
    </div>
</div>
