<?php

include '../include/header.php';
include '../fonctions/connexion_bdd.php';

$sponsor = $conn->query("SELECT * FROM llj_kata.sponsor");
?>
<style>
div.card {

  border: 2px solid #191970;
}
</style>
<div class ="container">

    <br>

    <h1>Sponsors</h1>
    <h5><i>Tout les sponsors de la ligue</i></h5>
    <hr>
    <?php
    $retour_ligne= -1;

    while ($affiche = $sponsor->fetch()){

        $retour_ligne = $retour_ligne +1;

        if($retour_ligne == 0){
            echo '<div class="card-deck">';
        }

        ?>


        <div class="card">
            <div class="card-body">
                <h5 class="title" align="center"><strong><?php echo $affiche['nom_sponsor']; ?></strong></h5>
                <hr>
                <p class="card-text"><U> Informations :</U>
                    <br>
                    <br>

                    &nbsp &nbsp &nbsp
                    Représentant : <?php echo $affiche['nom_representant'];?>
                    <br>
                    &nbsp &nbsp &nbsp
                    Téléphone : <?php echo $affiche['tel'];?>
                



            </div>
            <div class="card-footer">
                <small class="text-muted">  </small>
            </div>


        </div>
        <?php

        if($retour_ligne == 2){
            echo '</div>';
            echo '<br><hr><br>';
            $retour_ligne = -1;
        }
    }
    ?>

</div>